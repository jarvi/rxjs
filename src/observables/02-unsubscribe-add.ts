import { Observable, Observer, Subscriber } from "rxjs"

const intervalo$ = new Observable<number>(subscriber => {
  let count = 0
  let interval = setInterval(_ => {
    count++
    subscriber.next(count)
    console.log(count)
  }, 1000)

  return _ => {
    clearInterval(interval)
    console.log("intervalo destruido")
  }
})

let subs = intervalo$.subscribe(num => console.log(num))
let subs1 = intervalo$.subscribe(num => console.log(num))
let subs2 = intervalo$.subscribe(num => console.log(num))

setTimeout(_ => {
  subs.unsubscribe()
  subs1.unsubscribe()
  subs2.unsubscribe()
  console.log("%cproceso completado",'color:green')
}, 3000)
