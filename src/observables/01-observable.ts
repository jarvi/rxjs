import { Observable, Observer } from "rxjs"
//const obs$ = Observable.create()

const observer: Observer<any> = {
  next: success,
  error: error => console.warn("error: ", error),
  complete: () => console.info("proceso completado exitosamente"),
}

function success(value) {
  console.log("next:" + value)
}

const obs$ = new Observable<string>(subs => {
  subs.next("probando ando")
  /* subs.next(99999)
  subs.next(9*9)
  subs.next(NaN === NaN) */

  // forzar un error
  //   const a = undefined
  //   a.name = "x"
  subs.complete()
  subs.next("true")
})

//obs$.subscribe(console.log)

// obs$.subscribe(
//   valor => console.log('next: ', valor),
//   error => console.warn('error: ', error),
//   () => console.info('Completado')
// )

obs$.subscribe(observer)
